<?php
header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print('Спасибо, результаты сохранены.');
  }
  include('form.php');
  exit();
}

$errors = FALSE;
if (empty($_POST['fio'])) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}
if (empty($_POST['email'])) {
  print('Заполните электронную почту.<br/>');
  $errors = TRUE;
}
if (empty($_POST['birthyear'])) {
  print('Заполните год рождения.<br/>');
  $errors = TRUE;
}
if (!isset($_POST['radio2'])) {
  print('Выберите количество конечностей.<br/>');
  $errors = TRUE;
}
if (!isset($_POST['radio1'])) {
  print('Выберите пол.<br/>');
  $errors = TRUE;
}
if (empty($_POST['bio'])) {
  print('Заполните биографию.<br/>');
  $errors = TRUE;
}
if (empty($_POST['checkbox'])) {
  print('Подтверите согласие.<br/>');
  $errors = TRUE;
}

if ($errors) {
  exit();
}

$user = 'u20990';
$pass = '7645415';
$db = new PDO('mysql:host=localhost;dbname=u20990', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

$stmt = $db->prepare("INSERT INTO form (name,year,email,sex,limb,bio,checkbox) VALUES (:fio,:birthyear,:email, :sex,:limb,:bio,:checkbox )");
$stmt -> execute(array('fio'=>$_POST['fio'],'birthyear'=>$_POST['birthyear'],'email'=>$_POST['email'],'sex'=>$_POST['radio1'],'limb'=>$_POST['radio2'],'bio'=>$_POST['bio'],'checkbox'=>$_POST['checkbox']));

$form_id =  $db->lastInsertId();
$myselect = $_POST['super'];
if (!empty($myselect)) {
  foreach ($myselect as $ability) {
    if (!is_numeric($ability)) {
      continue;
    }
    $stmt = $db->prepare("INSERT INTO ability (form_id, ability_id) VALUES (:form_id, :ability_id)");
    $stmt -> execute(array(
      'form_id' => $form_id,
      'ability_id' => $ability
    ));  
  }
}



header('Location: ?save=1');

